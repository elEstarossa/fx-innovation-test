# REQUIREMENTS

DONE ✅ 1. Task List: Create a page that displays a list of tasks. Each task should have a name and a completion status.

DONE ✅ 2. Task Creation: Add a form or input field that allows users to create new tasks. When a new task is added, it should appear in the task list with a default completion status of "Not Completed."

DONE ✅ 3. Task Deletion: Allow users to delete tasks from the list.

DONE ✅ 4. Task Completion: Users should be able to mark tasks as completed or uncompleted by clicking on a checkbox or a button.

DONE ✅ 5. Styling: Implement basic styling to make the application visually appealing. You can use CSS, SCSS, or any CSSin- JS libraries like styled-components or emotion.

DONE ✅ 6. State Management: Use React state to manage the list of tasks and their completion statuses.

DONE ✅ 7. Component Structure: Organize your components logically. For example, you might have a TaskList component, a TaskItem component for each task, and a TaskForm component for creating new tasks.

DONE ✅ 8. Data Persistence (Optional): If you are up for an extra challenge, add local storage or integrate a simple backend (e.g., a JSON API) to persist task data.

DONE ✅ 9. Error Handling (Optional): Handle potential errors gracefully, such as displaying a message if the API request fails (if you implement data persistence).

DONE ✅ 10. Responsive Design (Optional): Make the application responsive to different screen sizes.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.