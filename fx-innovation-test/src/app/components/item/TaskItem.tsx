import React from "react";
import Item from "../../classes/item";
import './TaskItem.css';

interface TaskItemProps {
  task: Item;
  index: number;
  deleteTask: (index: number) => void;
  toggleCompletion: (index: number) => void;
}

function TaskItem({ task, index, toggleCompletion }: TaskItemProps) {
  return (
    <div className="item">
        <input
            type="checkbox"
            checked={task.status === "Completed"}
            onChange={() => toggleCompletion(index)}
        />
        <span className={task.status === "Completed" ? "completed" : "notCompleted"}>
            {task.name}
        </span>
    </div>
  );
}

export default TaskItem;
