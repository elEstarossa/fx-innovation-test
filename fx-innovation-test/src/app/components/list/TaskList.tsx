import { useEffect, useState } from "react";
import Item from "../../classes/item";
import TaskItem from "../item/TaskItem";
import TaskForm from "../form/TaskForm";
import './TaskList.css';

function TaskList() {
    const [tasks, setTasks] = useState<Item[]>([]);

    useEffect(() => {
        try {
            const storedTasks = JSON.parse(localStorage.getItem('tasks') || '[]');
            if (storedTasks.length > 0) {
                setTasks(storedTasks);
            }
            console.log('Data Loaded Succesfully');
        } catch (error) {
            console.error('Error loading tasks from local storage:', error);
        }
    }, []);

    useEffect(() => {
        try {
            localStorage.setItem('tasks', JSON.stringify(tasks));
            console.log('Data Saved Succesfully');
        } catch (error) {
            console.error('Error saving tasks to local storage:', error);
        }
    }, [tasks]);

    function addTask(name: string){
        const newItem = new Item(name, "Not Completed");
        setTasks([...tasks, newItem]);
    }

    function deleteTask(index:number) {
        const newTasks = [...tasks];
        newTasks.splice(index, 1);
        setTasks(newTasks);
    }

    function toggleCompleted(index: number){
        const newTasks = [...tasks];
        newTasks[index].status = newTasks[index].status === "Not Completed" ? "Completed" : "Not Completed";
        setTasks(newTasks);
    }

    return(
        <div className="container">
            <div className="task-list-container">
                <h1 className="task-list-header">Task List</h1>
                <div className="task-form">
                    <TaskForm addTask={addTask} />
                </div>
                <table className="task-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tasks.map((task, index) => (
                            <tr key={index}>
                                <td>
                                    <TaskItem
                                        task={task}
                                        index={index}
                                        deleteTask={deleteTask}
                                        toggleCompletion={toggleCompleted}
                                    />
                                </td>
                                <td>{task.status}</td>
                                <td>
                                    <button onClick={() => deleteTask(index)}>Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default TaskList;